package ee.bcs.valiit;

public class Skydiver extends Athlete {

    public void perform() {
        System.out.println(getName() + " is out of the plane, trying to survive ...");
    }

}
