package ee.bcs.valiit;

public class Runner extends Athlete {

    public Runner() {
        super();
    }

    public Runner(String name, String surname, int age) {
        super(name, surname, age);
    }

    @Override
    public void perform() {
        System.out.println(getName() + " is running ...");
    }
}
