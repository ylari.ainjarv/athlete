package ee.bcs.valiit;

import lombok.Data;

@Data
public abstract class Athlete {
    
    private String name;
    protected String surname;
    protected int age;
    protected Gender gender;
    protected double height;
    protected double weight;

    public Athlete() {}

    public Athlete(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
    
    public void perform() {}

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(name)
                .append(" ")
                .append(surname)
                .append(", ")
                .append(age)
                .append(", ")
                .append(gender)
                .append(", ")
                .append(height)
                .append(", ")
                .append(weight);
        return builder.toString();
    }

}
