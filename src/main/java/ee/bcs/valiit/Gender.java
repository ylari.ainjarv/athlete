package ee.bcs.valiit;

public enum Gender {
    FEMALE, MALE, UNKNOWN
}
