package ee.bcs.valiit;

public class AthleteApp {

    public static void main(String[] args) {
        Runner runner1 = new Runner("Mati", "Jooksja", 31);
        runner1.setGender(Gender.MALE);
        runner1.setHeight(180.0);
        runner1.setWeight(90.5);

        Skydiver skydiver1 = new Skydiver();
        skydiver1.setName("Lange");
        skydiver1.setSurname("Vari");
        skydiver1.setAge(25);
        skydiver1.setGender(Gender.FEMALE);
        skydiver1.setHeight(165.5);

        System.out.println(runner1);
        runner1.perform();

        System.out.println(skydiver1);
        skydiver1.perform();
    }

}
